<?php

namespace App\Services;

use Illuminate\Support\ServiceProvider;

class CustomResponseGenerator {

    public function generateResponseWithData($slug, $postData)
    {

        $data = $this->generateResponse($slug,$postData);
        $data['data'] = $postData;

        return $data;
    }

    public function generateResponse($slug,$postData=null)
    {

        /**
         * Initialize to 200
         */

        $data['status'] = 200;

        switch($slug){

            /**
             * Success Response Messages [200]
             */

            // .......


            case 'success':
                $data['message'] = 'Request was successfully processed.' .count($postData). ' items were retrieved';
                break;

            /*
             * Custom Errors [500]
             */

            case 'invalid-xml-url':
                $data['status'] = 500;
                $data['message'] = 'Invalid URL resource.';
                break;

            case 'error':
                $data['status'] = 500;
                $data['message'] = 'There was an unknown error. Please try again.';
                break;
        }

        return $data;

    }

}