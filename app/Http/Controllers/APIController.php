<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use XmlParser;

use App\Services\CustomResponseGenerator;

class APIController extends Controller
{
    public function __construct(CustomResponseGenerator $customResponseGenerator)
    {
        $this->cResponder = $customResponseGenerator;
    }

    public function parseURL(Request $request)
    {
        $input = $request->input();
        $data = [];

        // Check if url is valid
        if(!@file_get_contents($input['url'])) {
            return response()->json($this->cResponder->generateResponse('invalid-xml-url'));
        }
        // Get data from url
        $xmlObject = simplexml_load_file($input['url'], 'SimpleXMLElement', LIBXML_NOCDATA);

        // Convert to Array and extract attributes
        if($xmlObject->product) {
            foreach($xmlObject->product as $productIndex=>$obj) {
                array_push($data,(array)$obj);
                foreach($obj as $key=>$v) {
                    if($key=='price') {
                        $data[count($data)-1][$key] .= count($v->attributes()) ? ' '.$v->attributes()[0] : '';
                    } elseif($key=='categories') {
                        $data[count($data)-1][$key] = implode(' ',(array)$v);
                    }
                }
            }
        }

        if($data)  // Return data in json format
            return response()->json($this->cResponder->generateResponseWithData('success',$data));

        return response()->json($this->cResponder->generateResponse('error'));
    }

}
