## Tradetracker XML Parse - By Gene Ellorin

This project was developed by Gene Ellorin as part of the assessment process for tradetracker. 

## Requirements

The project was build using Laravel 5.4 framework + Jquery. Below are the requirements:

- PHP verion >= 7.0
- Git ( https://www.atlassian.com/git/tutorials/install-git )
- Composer ( https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx )

## Installation

After cloning the repository, follow the steps below to get it up running.

\1. Download a copy by cloning this repository ;

> git clone https://elloringene18@bitbucket.org/elloringene18/tradetracker.git

\2. Move to the new directory :

> cd tradetracker

\3. Apply the required permissions for the cache/log folders by giving the following directories a read and write permission ( 777 ):

- sudo chmod -R 777 storage/*
- sudo chmod -R 777 bootstrap/cache

\4. Install the dependencies for the framework by running the command on terminal :

> composer update

## How to use

Enter the URL on the text field then click on "Parse" to start the process and wait for the products to display. The loading time will depend on the amout of products being fetched.


## Developer Notes

I have tried to make it as simple as possible therefore there are just a few codes. The home.blade.php makes the request through AJAX and APIController does the fetching, validation and parsing then throws the response back to the home.blade.php. From there the view processes the response and displays the results. 

- APIController ( app\Http\Controllers )
- home.blade.php ( resources\views ) 

## Plugins used

- Bootstrap
- Jquery

## License

This a free/open source project.