<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gene Ellorin XML Parser for tradetracker</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/main.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="flex-center position-ref">
            <div id="globalAlert"></div>

            <div class="container">
                <div class="title m-b-md">
                    <h3>Tradetacker XML Parser by Gene Ellorin</h3>
                </div>
                <div class="body">
                    {!! Form::open(['route'=>'api.parse','id'=>'XMLForm']) !!}
                    {!! Form::input('text','url','http://pf.tradetracker.net/?aid=1&type=xml&encoding=utf-8&fid=251713&categoryType=2&additionalType=2&limit=10',['class'=>'form-control','id'=>'urlInput']) !!}
                    {!! Form::submit('Parse',['id'=>'submitBt']) !!}
                    {!! Form::close() !!}
                    <table class="table-striped" id="productsTable">
                        <tr>
                            <th class="text-center">#</th>
                            <th>Product ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Category</th>
                            <th>Product URL</th>
                            <th>Image URL</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        var productTable = $('#productsTable');
        var submitBt = $('#XMLForm').find('input[type=submit]');

        $('#XMLForm').on('submit', function(e){

            submitBt.addClass('loading').attr('disabled','disabled');
            $('.progress-bar').css('width','0');

            productTable.find('tr.product').remove('');
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {

                            /*
                                Generate table rows for the fetched data
                             */
                            $.each(response.data, function(index,row){
                                tableRow = '<tr class="product">';
                                tableRow += '<td class="text-center"><strong>'+(index+1)+'</strong></td>';
                                tableRow += '<td>'+row.productID+'</td>';
                                tableRow += '<td class="name">'+row.name+'</td>';
                                tableRow += '<td>'+row.description+'</td>';
                                tableRow += '<td>'+row.price+'</td>';
                                tableRow += '<td>'+row.categories+'</td>';
                                tableRow += '<td><a target="_blank" href="'+row.productURL+'" style="font-size:12px;">View Product</a></td>';
                                tableRow += '<td><img src="'+row.imageURL+'" width="50"></td>';

                                tableRow += '</tr>';

                                productTable.append(tableRow);
                            });
                        }

                        showMessage(response);
                    },
                    complete : function (){
                        processFlag = 1;

                        submitBt.removeClass('loading').removeAttr('disabled');
                    }
                });
            }
        });


        var globalAlert = $('#globalAlert');
        var my_timer;

        function alertFade() {
            my_timer = setTimeout(function () {
                globalAlert.animate({
                    'top' : '-150%',
                    'opacity' : '0',
                    'display' : 'none',
                }, 1000,function(){ $(this).attr('class','alert').text(''); });
            }, 5000);
        };

        function showMessage(response){
            clearTimeout(my_timer);

            $(globalAlert).dequeue().stop(true,true).finish().css('top','-150%');

            globalAlert.text(response.message).stop(true,true).css('margin-right',(-($(globalAlert).width())+'px'));

            if(response.status==200)
                globalAlert.animate({
                    'top' : '5px',
                    'opacity' : '1',
                    'display' : 'block',
                },50).attr('class','alert alert-success');
            else
                globalAlert.stop(true,true).animate({
                    'top' : '-150%',
                    'opacity' : '1',
                    'display' : 'block',
                },50).attr('class','alert alert-danger');

            alertFade();
        }

    </script>
    </body>
</html>
